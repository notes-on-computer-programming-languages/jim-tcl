# jim-tcl

Small footprint implementation of Tcl. http://jim.tcl.tk

## Official documentation
### Memory management
* [*Reference counting vs. tracing garbage collection*
  ](https://wiki.tcl-lang.org/page/Reference+counting+vs.+tracing+garbage+collection)

## Code contribution repositories (packages)
### [jimhttp](https://wiki.tcl-lang.org/page/jimhttp?R=0)
